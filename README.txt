********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: opensearchclient.module
Author: Robert Douglass <http://www.robshouse.net>

DESCRIPTION
********************************************************************
Turns your Drupal site into an OpenSearch client capable of searching
3rd party sites that offer OpenSearch RSS feeds. See http://opensearch.a9.com/
for more information.

RSS parsing is handled by the Magpie RSS library. This enables search
requests to remote sites to be cached in order to politely limit the amount
of overhead and traffic caused.

DEPENDENCIES
********************************************************************
This module depends on the search.module being enabled and on the
Magpie RSS library available from http://magpierss.sourceforge.net/

INSTALLATION
********************************************************************
Download magpierss-0.72.tar.gz
http://internap.dl.sourceforge.net/sourceforge/magpierss/magpierss-0.72.tar.gz

Unpack the archive into the opensearchclient directory so that you have
opensearchclient/magpierss-0.72/...

HOW TO USE THIS MODULES ONCE INSTALLED
********************************************************************
The module is currently capable of supporting only one search feed.
You can pick from a list of feeds at the A9 site:
http://opensearch.a9.com/-/search/moreColumns.jsp

The opensearchclient module should be capable of searching any of the
sites listed.

To configure the module to search a site, you need to determine what the
search URL template for that site is, and the corresponding placeholders for the
dynamic parts have to be set on the admin/settings/opensearchclient page.

Available placeholders are:
{searchTerms}
{startPage}
{startIndex}
{language}
{outputEncoding}
{count}

Here are some examples:

IceRocket
http://blogs.icerocket.com/search?q={searchTerms}&rss=1&os=1&p={startPage}&n={count}

Koders Source Code Search
http://blogs.icerocket.com/search?q={searchTerms}&rss=1&os=1&p={startPage}&n={count}

Syndic8 RSS and Atom Feed Search
http://www.syndic8.com/search_feeds/{startIndex}/{searchTerms}

A Drupal site with the OpenSearch module installed:
http://sitename.com/opensearch/node/{searchTerms}?page={startPage}

Once you have determined what the URL template for your target search site,
enter that value in the OpenSearch URL template field on the admin/settings/opensearchclient
page. Give the search a name in the Site name, field, and you should then see
an extra tab on your search page. Happy searching!

ATTENTION NUTCH USERS
*********************
The Nutch module integrates with the opensearchclient module to provide custom search result
rendering for Nutch specific elements (cache, explain, site, etc.)

http://drupal.org/project/nutch

Here is an example of a typical Nutch URL template:

http://www.example.com/opensearch?query={searchTerms}&start={startIndex}&hitsPerSite=2&hitsPerPage=10
